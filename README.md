## Contributing to the documentation



This documentation is automatically build. You are more than welcome to
contribute to this living document. Follow the steps below to add your
documentation:

### Pre-requirements
You will need python, git, and gnu make installed you our computer to
run the steps outlined below.

1. If you haven't already done so clone the repository:

```console
git clone https://gitlab.dkrz.de/ch1187/Information.git
```

2. Make sure that you have checkout out the main branch which is up to date:
```console
git checkout main
git pull origin main
```

3. If you haven't done so already install the dependencies:
```console
python -m pip install -r requirements.txt
```

4. Add you changes to `source` folder.
The page source in written in [rst format](https://sphinx-tutorial.readthedocs.io/step-1/)
or [md format](https://www.markdownguide.org/basic-syntax). If you want to
change text within existing files you can do so straight away.

If you decide to create a new document create a new file in the `source` folder.
You should chose the file name suffix `.md` if you choose md format or `.rst`
if you want to write the document in rst. For consistency reasons the largest
heading level should be 2 (`##`) in md and (`--`) in rst. Once finished editing
the file add the name of the file, *without* its extension into the
`source/index.rst` file.

5. Build the website locally
Use the
```console
make html
```
command to build the web site. Afterwards you can inspect the result in
`build/html/index.html`.

5. Create a new branch

After being finished do not commit your changes yet, but create a new branch
first.
```console
git checkout -b a_meaning_full_name
```

6. Add your changes

```console
git add any_files_that_may_have_been_added.rst
```

```console
git commit -a -m "A meaningful commit command"
```


7. Push your changes back to the gitlab repository
```console
git push origin a_meaning_full_name
```

8. Submit a merge request
After the `git push` command has been executed you will be displayed a
message asking if you would like to submit a merge request. You can follow
the link and create a new merge request via gitlab.

Write some information on what you have been doing in the notification field.
You can address team members by using the `@` sign for example `@k204230`. All
members that those members will get an email notification and can review your
changes. Once you have agreed on the necessary changes you can merge your
branch back into main.
