## Plugins: Online tools

### Freva
At NUKLEUS we provide a variety of small and large software tools that help
users to search for and analyse data. A corner stone of this software is the
Freva software. Covering all aspects of the Freva software is out of scope of
this documentation. For a detailed description please refer to the official
[freva documentation](https://freva.gitlab-pages.dkrz.de/evaluation_system/sphinx_docs/index.html)
page.

#### Command line interface
To activate freva on one of the *levante* login nodes hosted by the  German
Climate Computing Centre (DKRZ) simply type the following command:

```console
module load clint regiklim-ces
```
This command will make the freva software available to you.

#### RegIKlim freva web
Freva comes not only with a command line interface and python library but also
with a web based user interface (web UI). This web UI can be accessed via
[www-regiklim.dkrz.de](http://www-regiklim.dkrz.de). If you have any trouble
with either the web UI, the command line interface or the python library
please do not hesitate to contact us via [freva@dkrz.de](mailto:freva@dkrz.de).


### General Freva plugins
Freva cannot only search for datasets but also help you applying data
analysis plugins. The following bullet points offer an overview of the
available data analysis plugins:

- Animator: Animate data on lon/lat grids
- Blocking_2D: Calculate various 2D-Blocking Indices of the Northern Hemisphere
- ClimateChangeProfile: Calculate climate change signals.
- ClimDexCalc: Calculate the ETCCDI climate extreme indices
- Climpact: Process climate model data for input of impact model
- CWT: Calculate Circulation Weather Type by mean sea level pressure.
- PERCENTILE: Calculate multiple percentiles using CDO
- PSI: Calculate Precipitation Severity Index.
- WTRACK: Wind tracking algorithm.
- ZYKPAK: Cyclone tracking and identification package - applied to sea-level pressure -> psl

A detailed help of the available plugins can be found on the
[www-regkilim.dkrz help](https://www-regiklim.dkrz.de/plugins/about/) page.

If you want to get on board and develop your own plugins, we are more than happy
to support you. Still, a good starting point is the consultation of the
[freva documentation](https://freva.gitlab-pages.dkrz.de/evaluation_system/sphinx_docs/developers_guide.html)
and the [plugin template repository](https://gitlab.dkrz.de/freva/plugins4freva/plugintemplate.git).

### RegIKlim specific Freva plugins

Below is a list of software that is also made available when loading the freva
software:

- [gwl-loader](https://gitlab.dkrz.de/ch1187/plugins4freva/global-warming-level) To
calculate global warming level from cmip6 and cmip5 model output data.
- [rechunk-data](https://gitlab.dkrz.de/ch1187/rechunk-data): To rechunk
existing netcdf data. This tool helps chosing the optimal chunk size for your
data and hence can significantly improve data processing performance.
- [jupyter-kernel-install](https://gitlab.dkrz.de/k204230/install-kernelspec):
To install jupyter kernels for various programming languages into the users home.
If you are missing your favourite language please contact us at
[freva@dkrz.de](mailt:freva@dkrz.de)
- [metadata-inspector](https://gitlab.dkrz.de/freva/metadata-inspektor):
To inspect meta data of netcdf/grip/hdf5 and zarr data. This tool also supports
multiple files and data in entire directories.
