## Data

One of the corner stones of the NUKLEUS project is the provisioning of climate
model output data. Data access is provided via the [freva evaluation system](Software.html#Freva).
NUKLEUS provides a wide variety of data, not only regional
model output data. Besides climate model output data of different flavours, we
also offer different types of observational datasets. Altogether we offer
roughly 7 million data files spread across 8 data projects. Most prominent are
the *cmip6*, *cordex* and the *nukleus* data projects. To get a better overview
you can use the [freva databrowser](https://www-regiklim.dkrz.de/solr/data-browser/).

### Available Data

The most important datasets available in the
[freva databrowser](https://www-regiklim.dkrz.de/solr/data-browser/) are briefly outlined here:

- *cordex* : This data project holds all [cordex data](https://cordex.org/)
available at the German Climate Computing Centre (DKRZ).
- *cordex-nukleus*: A recommended subset of cordex models that represent
the German climate specifically well. The subset consists of 25 different model
members. Those members consist of output from 5 global models that has been
downscaled by 5 different regional models. The resulting ensemble reflects the
best estimate of uncertainty and accuracy at the given cordex simulation scale
(roughly 11 km) and domain (central Europe).
- *nukleus*: This data project is the heart of the NUKLEUS project and hosts
the model output of the high resolution 3 km historical and climate change
scenario simulations. As currently most of the simulations are under way, this
data project will be filled consecutively. The data project has also a number of
bias corrected products, which can generally be seen as more realistic than
their non-corrected counter parts.
- *reanalysis*: This data project offers a variety of
[reanalysis products](https://en.wikipedia.org/wiki/Atmospheric_reanalysis).
Most prominent is the
[ERA5 Reanalysis Dataset](https://www.ecmwf.int/en/forecasts/datasets/reanalysis-datasets/era5)
at a global resolution of 30 km in space and 1 hour in time.
- *observations*: Various observational datasets such
as gridded radar based precipitation estimates (RADOLAN).

#### Currently Available NUKLEUS Simulations

### Data Access

### Production and quality check of NUKLEUS data

### How to apply data in impact models



---
__Note__: Bias corrected data can be distinguished by the **b** suffix within
the product name. For example GER-11b would be bias corrected data of the
cordex data across Germany (GER) at 11 km.