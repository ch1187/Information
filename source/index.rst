.. Nukleus documentation master file, created by
   sphinx-quickstart on Tue Jul 19 13:49:31 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Information about Nukleus
========================

The joint project Nukleus is a cross-sectional activity of the
funding measure RegIKlim. The project aims at addressing
the generation, evaluation and provision of regional and local climate
information for the model regions in RegIKlim. With this
high-resolution information it creates the data basis for a publicly
accessible data portal in the future. In order to guarantee an
optimal applicability of the data, the project simultaneously develops
interfaces for the integration of spatially and temporally high-resolution
climate information into the impact models of the individual model regions.

Here you will find general information on data, software, and many other Questions
related to Nukleus.

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   Overview
   Data
   Software
   FAQ
..   Contributing




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
