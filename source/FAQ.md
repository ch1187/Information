## Frequently Ask Questions (FAQ)

### Q.1 How can I get access to freva and the Nukleus data herein?
You will need an account with the German Climate Computing Centre (DKRZ). If
you aren't in possession of such an account please register at the
[luv.dkrz.de](https://luv.dkrz.de/projects/newuser/) help page.
After you have gotten access to your DKRZ user name you can request membership
of the ch1187 group. To do so, visit [luv.dkrz.de](https://luv.dkrz.de/projects/ask/)
and search for *1187* in the *Project* dropdown menu. Remember to write
a meaningful text so that we can better assign your request.

### Q.2 How can I login to the DKRZ computing nodes?
If you have an account with DKRZ you can do so by using your favorite ssh
client. For example:

```console
ssh b12345@levante.dkrz.de
```
**Note:** The new login nodes are called *levante* instead of the old
*regiklim* host names.

### Q.3 How can I get access to the freva software?
You can either use the freva command line interface (cli) by logging on to
the DKRZ *levante* login node (Q.2) and do a

```console
module load clint regiklim-ces
```
After this the `freva` command will be available to you.
An alternative to the cli is the web user interface (webUI)
on [www-regiklim.dkrz.de](http://www-regiklim.dkrz.de). If you can't logon to
the website please make sure you are part of the ch1187 group. See also
Q.1.

### Q.4 How can I use freva on jupyter?
To be able to use freva within jupyter you must logon to the DKRZ *levante*
computing resources (Q.2) and load the `regiklim-ces` module (Q.3).
After that you will have to install a your jupyter kernel. For example

```
jupyter-kernel-install python --name regiklim --display-name RegIKlim
```

This will install a jupyter python kernel that allows you to load freva from
within a jupyter notebook. Note: You only have to execute this command once.

After that, you can load the kernel on the [jupyter-hub](https://jupyterhub.dkrz.de/hub/home).

### Q.5 Where can I get more information on freva?
Detailed freva documentation is available via the
[freva dos](https://freva.gitlab-pages.dkrz.de/evaluation_system/sphinx_docs/index.html)
page.


### Q.6 Where can I get more information on the DKRZ computing resources?
Detailed information on all DKRZ services is available at the [dkrz docs](https://docs.dkrz.de/)

### Q.7 Where can I find documentation of the available freva plugins?
For help on freva plugins visit the [help section](https://www-regiklim.dkrz.de/plugins/about/)
of the freva web user interface.

### Q.8 Where can I get help?
If you need help and have exhausted any available documentation feel free to
drop us an email: [freva@dkrz.de](mailto:freva@dkrz.de)
