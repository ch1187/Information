## Goals and plans

The main goal of NUKLEUS is the generation and provision of useful and usable weather 
and climate information with high spatial and temporal resolution for Germany. Such 
information can be used to make reliable statements about regional and local climate 
change adaptation and mitigation strategies.
In the first project phase (2020-2023), NUKLEUS has been developing an ensemble of 
high-resolution climate simulation scenarios. In the ongoing second phase, the focus 
lies on the development/conceptualisation of a prototype for a so-called climate register 
(Klimakataster), together with the other RegIKlim project WIRKsam. At the end of the 
second NUKLEUS phase, the prototype climate register will be made available as a climate 
information and data platform to various stakeholders in the field of climate change 
adaption (e.g. research institutes, municipalities, civil protection). The climate 
register is intended to support with collecting and processing of relevant data, as well 
as to offer transfer and networking opportunities for climate services.


### Work package 1
Work package 1 focuses on the development of the climate register. NUKLEUS will primarily 
be responsible for the backend of the register, while WIRKsam will supervise the frontend 
development. In a first step, NUKLEUS and WIRKsam will carry out an analysis of needs, 
connectivity and challenges for the register. Based on these analyses, NUKLEUS will 
support WIRKsam in developing an overall concept for the climate register. Additionally, 
NUKLEUS will prepare guidelines and tutorials to enable an effective use of the climate 
register in the model regions and by external partners.

### Work package 2
Work package 2 focuses on the provision of evaluated and quality-controlled climate data, 
which are optimised for the climate register and for users. The first task deals with the 
evaluation of the NUKLEUS ensemble and the computation of climate change signals. Focus 
lies on the quality, bandwidth and added value of the model data. The second task will 
investigate the robustness of the climate change signals, while task 3 will prepare the 
climate data in a user-friendly way. This includes a standardised bias correction, which 
could also be transferred to the impact modellers and the climate register. In addition, 
work package 2 will work on a method to statistically generate datasets with ultra-high 
spatial and temporal resolution for certain applications (e.g. heavy precipitation events).

### Work package 3
The aim of work package 3 is to bring the results and data from work package 2 as well 
as those from external partners into action. For this purpose, the Freva system was 
established in the first funding phase. Especially the Freva plug-ins have proven to be 
very useful for the interface to users in order to support various impact modellers 
(e.g. in environmental, planning or engineering offices) in their data-driven work. In 
the second NUKLEUS phase, the Freva system will be optimised and additional, more 
complex, plug-ins will be developed. In addition to the climate model data, focus will 
also be given to geo data.
